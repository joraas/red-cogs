import discord
import urllib3
import json
import logging
import time
from redbot.core import commands, checks, Config
from redbot.core.utils.chat_formatting import info, error
import asyncio
from typing import Union

urllib3.util.ssl_.DEFAULT_CIPHERS = "DEFAULT@SECLEVEL=1"

log = logging.getLogger('red.apitools')


class Apistatus(commands.Cog):
    """Status channel \n 🟢 🔴"""

    def __init__(self, bot):
        self.bot = bot
        self.config = Config.get_conf(self, identifier=69)
        default_guild = {
            'token': None,
            'url': None,
            'channel_ids': []
        }
        default_global = {
            'active_guilds': []
        }
        default_channel = {
            'namespace': None,
            'online': "online",
            'offline': "offline"
        }
        self.config.register_guild(**default_guild)
        self.config.register_global(**default_global)
        self.config.register_channel(**default_channel)
        self.to_stop = []


    async def _loop(self):
        while True:
            guilds = await self.config.active_guilds()
            if guilds:
                for guildid in guilds:
                    guild = self.bot.get_guild(guildid)
                    if not guild:
                        break
                    result = await self._apicall(guild)
                    if result:
                        # Perform actions
                        for cid in (await self.config.guild(guild).channel_ids()):
                            channel = self.bot.get_channel(cid)
                            namespace = (await self.config.channel(channel).namespace())
                            status = result[namespace][0]
                            online = await self.config.channel(channel).online()
                            offline = await self.config.channel(channel).offline()
                            if status == 1:
                                await channel.edit(name=online)
                            else:
                                await channel.edit(name=offline)
                    
                    log.info("Updated all api status channels on {}".format(guild.name))
            
            # Sleep for next loop
            await asyncio.sleep(30)


    @commands.command()
    @checks.admin()
    async def set_token(self, ctx, token):
        """Set server-wide token variable"""

        await self.config.guild(ctx.guild).token.set(token)
        await ctx.send(info("Token variable set server-wide"))


    @commands.command()
    @checks.admin()
    async def set_url(self, ctx, url):
        """Set server-wide url variable"""

        await self.config.guild(ctx.guild).url.set(url)
        await ctx.send(info("URL variable set server-wide"))


    @commands.command()
    @checks.admin()
    async def add_channel(self, ctx, namespace, channel: Union[discord.TextChannel, discord.VoiceChannel]=None):
        """Add channel to visualize status to channel: <namespace> [channel]"""

        if channel is None:
            channel = ctx.channel
        
        t = await self.config.guild(ctx.guild).channel_ids()
        if type(t) is list:
            if not channel.id in t:
                t.append(channel.id)
            else:
                # In case of duplicates
                t = list(filter(lambda a: a != channel.id, t))
                t.append(channel.id)
        else:
            t = list()
            t.append(channel.id)

        await self.config.guild(ctx.guild).channel_ids.set(t)
        await self.config.channel(channel).namespace.set(namespace)
        
        await ctx.send(info("Channel {} added".format(channel.id)))


    @commands.command()
    @checks.admin()
    async def remove_channel(self, ctx, channel: Union[discord.TextChannel, discord.VoiceChannel]=None):
        """Remove channel from visualization list: <channel>"""

        if channel is None:
            channel = ctx.channel

        t = (await self.config.guild(ctx.guild).channel_ids())
        if type(t) is list:
            t.remove(channel.id)
        else:
            t = list()
            
        await self.config.guild(ctx.guild).channel_ids.set(t)
        await self.config.channel(channel).namespace.set(None)

        await ctx.send(info("Channel {} removed".format(channel.id)))


    @commands.command()
    @checks.admin()
    async def list_channels(self, ctx):
        """List active channels"""

        all_channels = []
        for channelid in (await self.config.guild(ctx.guild).channel_ids()):
            channel = self.bot.get_channel(channelid)
            t = {
                'Name': channel.name,
                'Namespace': await self.config.channel(channel).namespace(),
                'Online': await self.config.channel(channel).online(),
                'Offline': await self.config.channel(channel).offline()
            }
            all_channels.append(t)
        await ctx.send(info("Active channels list:\n {}".format(json.dumps(all_channels, indent=4, sort_keys=True))))


    @commands.command()
    @checks.admin()
    async def activate(self, ctx):
        """Activate visualization server-wide"""

        t = (await self.config.active_guilds())
        if type(t) is list:
            if not ctx.guild.id in t:
                t.append(ctx.guild.id)
            else:
                # In case of duplicates
                t = list(filter(lambda a: a != ctx.guild.id, t))
                t.append(ctx.guild.id)
        else:
            t = list()
            t.append(ctx.guild.id)

        await self.config.active_guilds.set(t)
        await ctx.send(info("API status checker activated for server"))


    @commands.command()
    @checks.admin()
    async def deactivate(self, ctx):
        """Deactivate visualization server-wide"""

        t = (await self.config.active_guilds())
        if type(t) is list:
            t.remove(ctx.guild.id)
        else:
            t = list()
            
        await self.config.active_guilds.set(t)
        await ctx.send(info("API status checker dectivated for server"))


    @commands.command()
    @checks.admin()
    async def list_guilds(self, ctx):
        """List active guilds"""

        all_guilds = []
        for guildid in (await self.config.active_guilds()):
            guild = self.bot.get_guild(guildid)
            t = {
                'Name': guild.name,
            }
            all_guilds.append(t)
        await ctx.send(info("Active channels list:\n {}".format(json.dumps(all_guilds, indent=4, sort_keys=True))))


    @commands.command()
    @checks.admin()
    async def set_online_offline(self, ctx, online, offline, channel: Union[discord.TextChannel, discord.VoiceChannel]=None):
        """Set online/offline status name of channel: <online> <offline> [channel]"""
        
        if channel is None:
            channel = ctx.channel
            
        await self.config.channel(channel).online.set(online)
        await self.config.channel(channel).offline.set(offline)
        
        await ctx.send(info("Status names of channel {} set to {} and {}".format(channel.id, online, offline)))


    @commands.command()
    @checks.admin()
    async def manual_update(self, ctx, channel: Union[discord.TextChannel, discord.VoiceChannel], namespace):
        """Manual status update. <channel id> <namespace>"""

        result = self._apicall(ctx.guild)

        if result:
            # Perform actions
            errors = result[namespace][0]
            log.info("Result {}\n turned into errors {}".format(result, errors))
            if errors:
                await channel.edit(name="status-offline")
                log.info("Setting name on channel {} to {}".format(channel.id, "status-offline"))
            else:
                await channel.edit(name="status-online")
                log.info("Setting name on channel {} to {}".format(channel.id, "status-online"))


    async def _apicall(self, guild: discord.Guild):
        if guild.id in self.to_stop:
            self.to_stop.remove(guild)
            return

        # Perform apicall
        url = await self.config.guild(guild).url()
        token = await self.config.guild(guild).token()
        headers = {}

        if token:
            headers = {"Authorization": "Bearer {}".format(token)}
        
        http = urllib3.PoolManager()
        request = http.request('GET', url, headers=headers)
        result = None
        if request.status == 200:
            result = json.loads(request.data.decode('utf-8'))
        else:
            log.error("There was an error with code {} contacting the api at {}".format(request.status, url))
        # Process result
        return result