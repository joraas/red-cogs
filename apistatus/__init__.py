from redbot.core.bot import Red
from .apistatus import Apistatus
import asyncio

async def setup(bot: Red):
    instance = Apistatus(bot)
    bot.add_cog(instance)
    bot.loop.create_task(instance._loop())